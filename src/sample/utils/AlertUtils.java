package sample.utils;

import javafx.scene.control.Alert;
//弹出框操作类
public class AlertUtils {
    /**
     * @param title 对话框标题
     * @param headerText 对话框内容
     * @param contentText 对话框底部
     * information类型的对话框
     */
    public static void informationAlert(String title,String headerText,String contentText){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
    /**
     * @param title 对话框标题
     * @param headerText 对话框内容
     * @param contentText 对话框底部
     * Warning类型的对话框
     */
    public static void warningAlert(String title,String headerText,String contentText){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
    /**
     * @param title 对话框标题
     * @param headerText 对话框内容
     * @param contentText 对话框底部
     * Error类型的对话框
     */
    public static void errorAlert(String title,String headerText,String contentText){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

}
