package sample.model;

public class Stock {
    private Integer id;
    //缩略图地址
    private String photoPath;
    //商品名
    private String goodsName;
    //单价
    private String danjia;
    //数量
    private Integer number;
    //适用性别
    private String sex;
    //适用季节
    private String jijie;

    public Stock(Integer id, String photoPath, String goodsName, String danjia, Integer number, String sex, String jijie) {
        this.id = id;
        this.photoPath = photoPath;
        this.goodsName = goodsName;
        this.danjia = danjia;
        this.number = number;
        this.sex = sex;
        this.jijie = jijie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getDanjia() {
        return danjia;
    }

    public void setDanjia(String danjia) {
        this.danjia = danjia;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getJijie() {
        return jijie;
    }

    public void setJijie(String jijie) {
        this.jijie = jijie;
    }
}
