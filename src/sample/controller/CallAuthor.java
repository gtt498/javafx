package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import sample.utils.AlertUtils;

import java.net.URL;
import java.util.ResourceBundle;

//界面控制器必须实现自接口Initializable
public class CallAuthor implements Initializable {
    public static AlertUtils AlertUtils = new AlertUtils();
    @FXML
    private Menu about;//其中控件类型为JavaFX的控件类名，控件编号取自fxml文件中的fx:id
    public static void callAuthor(ActionEvent event){

        AlertUtils.informationAlert("联系作者","qq：969516695\n" +
                "添加作者请备注xx进销存系统-使用反馈\n" +
                "或备注xx进销存系统-商务合作\n","");
        System.out.println("联系作者按钮被响应了");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
