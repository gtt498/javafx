package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import sample.model.Stock;
import sample.utils.AlertUtils;

import java.net.URL;
import java.util.ResourceBundle;

//界面控制器必须实现自接口Initializable
public class InitStock implements Initializable {
    @FXML
    private Menu about;//其中控件类型为JavaFX的控件类名，控件编号取自fxml文件中的fx:id
    public static void initStock(ActionEvent event){
        Stock stock = new Stock(1,"x","童装","10元",10,"男装","春秋");


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
